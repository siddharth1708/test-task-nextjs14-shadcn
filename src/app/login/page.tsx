"use client";
import LoginSkeleton from "@/components/Login/skeleton";
import dynamic from "next/dynamic";
import { Suspense, useEffect, useState } from "react";
const Login = dynamic(() => import("@/components/Login"), { ssr: false });

const Page = () => {
  const [loginLoaded, setLoginLoaded] = useState(false);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setLoginLoaded(true);
    }, 1000);
    return () => clearTimeout(timeoutId);
  }, []);

  return (
    <Suspense fallback={<LoginSkeleton />}>
      {loginLoaded ? <Login /> : <LoginSkeleton />}
    </Suspense>
  );
};

export default Page;
