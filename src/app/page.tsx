import { Logout } from "@/components/Logout";
import { Button } from "@/components/ui/button";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center gap-8 p-24">
      <Button>Hello, Good Morning!</Button>
      <Logout />
    </main>
  );
}
