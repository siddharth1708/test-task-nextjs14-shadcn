const AppRoutes = {
    LOGIN: '/login',
    SIGNUP: '/signup',
    HOME: '/'
}

export const GUEST_ROUTES = [AppRoutes.LOGIN, AppRoutes.SIGNUP]

export default AppRoutes