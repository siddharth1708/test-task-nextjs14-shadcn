import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { GUEST_ROUTES } from "./contants/AppRoutes";

export function middleware(request: NextRequest) {
  const token = request.cookies.get("auth_token")?.value;
  const path = request.nextUrl.pathname;
  if (GUEST_ROUTES.includes(path) || path.includes("next")) {
    return NextResponse.next();
  }
  if (token) {
    if (!GUEST_ROUTES.includes(path)) {
      return NextResponse.next();
    } else {
      return NextResponse.redirect(new URL("/", request.url));
    }
  } else {
    return NextResponse.redirect(new URL("/login", request.url));
  }
}
