export type GlobalState = {
  errors: string[];
  errorMessage: string;
  success: boolean;
  successMessage: string;
  maintenance: boolean;
};
