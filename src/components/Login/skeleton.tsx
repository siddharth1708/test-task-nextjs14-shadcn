import React from "react";
import { Skeleton } from "../ui/skeleton";

const LoginSkeleton = () => {
  return (
    <div className="min-h-screen min-w-screen flex justify-center items-center">
      <div className="max-w-[100%] p-4 w-[400px]">
        <div className="flex flex-col ">
          <Skeleton className="h-[17px] w-[150px] rounded-xl mb-1" />
          <Skeleton className="h-10 w-full rounded-xl" />

          <Skeleton className="mt-8 h-[17px] w-[150px] rounded-xl mb-1" />
          <Skeleton className="h-10 w-full rounded-xl" />

          <Skeleton className="mt-8 h-10 w-full" />
        </div>
      </div>
    </div>
  );
};

export default LoginSkeleton;
