"use client";

import { LoginForm } from "@/Forms/Auth/Login";

const Login = () => {
  return (
    <main className="min-h-screen min-w-screen flex justify-center items-center">
      <section className="max-w-[100%] p-4 w-[400px]">
        <LoginForm />
      </section>
    </main>
  );
};

export default Login;
