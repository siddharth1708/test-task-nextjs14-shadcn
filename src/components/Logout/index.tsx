"use client";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { useAppDispatch } from "@/store";
import { resetAuthData, setAuthData } from "@/store/authentication";
import { TOKEN } from "@/utils/cookies";
import cookie from "js-cookie";
import { useRouter } from "next/navigation";

export function Logout() {
  const dispatch = useAppDispatch();
  const router = useRouter();

  function handleLogout() {
    cookie.remove(TOKEN);
    dispatch(resetAuthData());
    dispatch(setAuthData({ authCheck: true }));
    router.replace("/login");
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="outline">Logout</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Logout</DialogTitle>
          <DialogDescription>{`Are you ready to log out?`}</DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            onClick={() => {
              handleLogout();
            }}
          >
            Yes
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
